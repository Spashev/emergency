from aiogram.types import ReplyKeyboardMarkup, KeyboardButton


def get_keyboard_emergency() -> ReplyKeyboardMarkup:
    return ReplyKeyboardMarkup(
        keyboard=[
            [
                KeyboardButton(text="Да"),
                KeyboardButton(text="Нет")
            ]
        ],
        resize_keyboard=True
    )


def get_keyboard_emergency_type() -> ReplyKeyboardMarkup:
    return ReplyKeyboardMarkup(
        keyboard=[
            [
                KeyboardButton(text="Пожар"),
                KeyboardButton(text="Паводок"),
                KeyboardButton(text="Другое")
            ]
        ],
        resize_keyboard=True
    )


def get_keyboard_location() -> ReplyKeyboardMarkup:
    return ReplyKeyboardMarkup(
        keyboard=[
            [
                KeyboardButton(text="Поделиться", request_location=True)
            ]
        ],
        resize_keyboard=True
    )
