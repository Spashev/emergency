from aiogram import types
from aiogram.enums import ParseMode


async def handle_start(message: types.Message) -> None:
    await message.answer(text=f"Добро пожаловать, {message.from_user.full_name}! Вас приветствует бот помощник.")


async def handle_help(message: types.Message) -> None:
    text = "[Отправьте фото в группу или напишите боту](https://t.me/gemaplaybot) что у вас случилось!"
    await message.answer(text=text, parse_mode=ParseMode.MARKDOWN)
