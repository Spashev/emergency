import os
import logging
from dotenv import load_dotenv

from aiogram import Router
from aiogram import types, F
from aiogram.fsm.context import FSMContext

from app.state.message import CreateMessage
from app.keyboards import emergency_keyboard
from app.utils.forward import forward_message, send_location

router = Router()


@router.message(F.photo)
async def handle_photo(message: types.Message, state: FSMContext) -> None:
    user_name = message.from_user.full_name

    print(user_name, message.from_user.id)

    photo = message.photo[-1].file_id
    await state.update_data(msg_photo=photo, msg_text=message.md_text)
    await message.answer(
        text=f"{user_name}, вы отправили фото.\nЭто чрезвычайная ситуация?",
        reply_markup=emergency_keyboard.get_keyboard_emergency()
    )
    await state.set_state(CreateMessage.get_emergency_keyboard_text)


@router.message(CreateMessage.get_emergency_keyboard_text, lambda message: message.text in ["Да", "Нет"])
async def handle_emergency(message: types.Message, state: FSMContext) -> None:
    await state.update_data(emergency_text=message.text)
    await message.answer(
        text="Выберите тип чрезвычайной ситуации:",
        reply_markup=emergency_keyboard.get_keyboard_emergency_type()
    )
    await state.set_state(CreateMessage.get_disaster_type_keyboard_text)


@router.message(CreateMessage.get_disaster_type_keyboard_text,
                lambda message: message.text in ["Пожар", "Паводок", "Другое"])
async def handle_disaster_type(message: types.Message, state: FSMContext) -> None:
    await state.update_data(disaster_type_text=message.text)
    await message.answer(
        text="Нужно определить местоположение.",
        reply_markup=emergency_keyboard.get_keyboard_location()
    )
    await state.set_state(CreateMessage.get_location)


@router.message(CreateMessage.get_location, F.location)
async def handle_location(message: types.Message, state: FSMContext):
    lat = message.location.latitude
    lon = message.location.longitude
    reply = f"latitude:  {lat}\nlongitude: {lon}"

    await state.update_data(location_text=reply)
    await state.update_data(location=message.location)
    await message.answer(
        text="Добавьте описание:"
    )
    await state.set_state(CreateMessage.confirm)


@router.message(CreateMessage.confirm)
async def handle_description(message: types.Message, state: FSMContext) -> None:
    await state.update_data(description_text=message.md_text)
    await message.answer(
        text="Спасибо за ответы. С Вами в ближайшие время свяжутся."
    )

    data = await state.get_data()

    location = data.get('location')
    message_from_user_id = message.from_user.id
    message = f"Это чрезвычайная ситуация?: {data.get('emergency_text')}\nТип чрезвычайной ситуации: {data.get('disaster_type_text')}\nМестоположение: {data.get('location_text')}\nОписание: {data.get('description_text')}"

    await forward_message(
        message_from_user_id,
        data.get('msg_photo', 'N/A'),
        message
    )

    await send_location(
        message_from_user_id,
        location.latitude,
        location.longitude,
    )
