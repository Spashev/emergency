def create_table(conn, cursor):
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY,
            question_number INTEGER,
            username TEXT NOT NULL,
            member_id TEXT NULL,
            is_emergency BOOLEAN DEFAULT FALSE,
            type_emergency TEXT,
            description TEXT,
            image_path TEXT NULL,
            processed BOOLEAN DEFAULT FALSE
        )
    """)

    conn.commit()


def create_member(cursor, conn, member_id, user_name):
    cursor.execute("""INSERT INTO users (
            member_id, 
            username, 
            question_number
            ) VALUES (
            ?, ?, ?
        )""", (member_id, user_name, 1))
    conn.commit()


def select_user_by_member_id(cursor, member_id):
    cursor.execute(
        'SELECT * FROM users WHERE member_id=?',
        (member_id,)
    )

    return cursor.fetchall()


def update_emergency_status(cursor, conn, member_id, message):
    cursor.execute(
        'UPDATE users SET is_emergency = ?, question_number=? WHERE member_id = ?',
        (message == "Да", 2, member_id)
    )
    conn.commit()


def update_emergency_type(cursor, conn, member_id, message):
    cursor.execute(
        'UPDATE users SET type_emergency = ?, question_number=? WHERE member_id = ?',
        (message, 3, member_id)
    )
    conn.commit()


def update_description(cursor, conn, member_id, message):
    cursor.execute(
        'UPDATE users SET description = ?, question_number = 4 WHERE member_id = ?',
        (message, member_id)
    )
    conn.commit()
