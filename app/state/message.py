from aiogram.fsm.state import StatesGroup, State


class CreateMessage(StatesGroup):
    get_text = State()
    get_photo = State()
    get_emergency_keyboard_text = State()
    get_disaster_type_keyboard_text = State()
    get_description = State()
    get_location = State()
    confirm = State()
