import os
from dotenv import load_dotenv

from aiogram import Bot, Dispatcher
from aiogram.enums import ParseMode
from aiogram.filters import CommandStart, Command
from aiogram.client.bot import DefaultBotProperties

from app.handlers import commands
from app.handlers import bot_router
from app.utils.context import BotContext

load_dotenv()

bot_token = os.getenv("BOT_TOKEN")
bot = Bot(token=bot_token, default=DefaultBotProperties(parse_mode=ParseMode.HTML))
dp = Dispatcher()
BotContext.set_bot(bot)


def set_handlers():
    dp.message.register(commands.handle_start, CommandStart())
    dp.message.register(commands.handle_help, Command(commands=['help']))
    dp.include_router(bot_router)


async def main() -> None:
    set_handlers()
    await dp.start_polling(bot)
