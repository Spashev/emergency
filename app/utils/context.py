class BotContext:
    _bot = None

    @classmethod
    def set_bot(cls, bot):
        cls._bot = bot

    @classmethod
    def get_bot(cls):
        return cls._bot
