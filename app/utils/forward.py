from aiogram import types
from aiogram.enums import ParseMode

from app.utils.context import BotContext


async def forward_message(
        chat_id,
        photo: str = None,
        text: str = '',
):
    bot = BotContext.get_bot()
    await bot.send_photo(
        chat_id=chat_id,
        caption=text,
        photo=photo,
        parse_mode=ParseMode.MARKDOWN
    )


async def send_location(
        chat_id,
        latitude,
        longitude,
):
    bot = BotContext.get_bot()
    await bot.send_location(
        chat_id=chat_id,
        latitude=latitude,
        longitude=longitude,
    )
