import sys
import logging
import asyncio
from app import start

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(start.main())
